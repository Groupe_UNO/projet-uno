package Default;

import javax.swing.*;
import java.util.ArrayList;

public class MainJoueur {

    private ArrayList<Carte> mainJoueur1 = new ArrayList<Carte>(108);
    private ArrayList<Carte> mainJoueur2 = new ArrayList<Carte>(108);
    private ArrayList<Carte> mainJoueur3 = new ArrayList<Carte>(108);
    private ArrayList<Carte> mainJoueur4 = new ArrayList<Carte>(108);

    MainJoueur() {

    }

    //***** Getters et Setters *****//


    public String [] getMainJoueur1() {
        String tabCartes[] = new String [108];
        for (int i=0; i<mainJoueur1.size(); i++){
            tabCartes[i] = mainJoueur1.get(i).getNomImage();
        }
        return tabCartes;
    }

    public void setMainJoueur1(ArrayList<Carte> mainJoueur1) {
        this.mainJoueur1 = mainJoueur1;
    }

    public String [] getMainJoueur2() {
        String tabCartes[] = new String [108];
        for (int i=0; i<mainJoueur2.size(); i++){
            tabCartes[i] = mainJoueur2.get(i).getNomImage();
        }
        return tabCartes;
    }

    public void setMainJoueur2(ArrayList<Carte> mainJoueur2) {
        this.mainJoueur2 = mainJoueur2;
    }

    public String[] getMainJoueur3() {
        String tabCartes[] = new String [108];
        for (int i=0; i<mainJoueur3.size(); i++){
            tabCartes[i] = mainJoueur3.get(i).getNomImage();
        }
        return tabCartes;
    }

    public void setMainJoueur3(ArrayList<Carte> mainJoueur3) {
        this.mainJoueur3 = mainJoueur3;
    }

    public String[] getMainJoueur4() {
        String tabCartes[] = new String [108];
        for (int i=0; i<mainJoueur4.size(); i++){
            tabCartes[i] = mainJoueur4.get(i).getNomImage();
        }
        return tabCartes;
    }

    public void setMainJoueur4(ArrayList<Carte> mainJoueur4) {
        this.mainJoueur4 = mainJoueur4;
    }
}