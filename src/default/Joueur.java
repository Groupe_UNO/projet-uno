package Default;

import java.util.ArrayList;

public class Joueur {
    //Attributs

    public String nomJoueur;

    private ArrayList<Carte> cartesJoueur = new ArrayList<Carte>(108);



    // Constructeur
    public Joueur(String nom) {
        this.nomJoueur = nom;
    }


    // ***** Getters et Setters ***** //

    public ArrayList<Carte> getCartesJoueur() {
       /* String[] paquetCartes = new String [108];
        for (int i =0; i<cartesJoueur.size();i++){
            paquetCartes[i] = cartesJoueur.get(i).getNomImage();
        }*/
        return cartesJoueur;
    }

    public void setCartesJoueur(ArrayList<Carte> cartesJoueur) {
        this.cartesJoueur = cartesJoueur;
    }

    public String getNomJoueur() {
        return nomJoueur;
    }

    public void setNomJoueur(String nomJoueur) {
        this.nomJoueur = nomJoueur;
    }
}
