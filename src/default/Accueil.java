package Default;

/**
 * *
 * @author sokhnafatoudieng
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Accueil extends JFrame implements MouseListener {
    private String nomJoueur;
    private final int hauteur;
    private final int largeur;
    private Image accueil;

    private static JTextField pseudo = new JTextField(); // Création de la zone de saisie du pseudo


    // Déclaration du constructeur
    public Accueil(){
        this.hauteur = 800; /*Définition de la hauteur de la fenêtre de jeu*/
        this.largeur = 1100; /*Définition de la largeur de la fenêtre de jeu*/
        this.setTitle("Uno Lion"); /*Définition du titre de la fenêtre de jeu*/
        this.setSize(largeur, hauteur);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        setResizable(false);
        //Changer l'image de l'icone
        Image icone = Toolkit.getDefaultToolkit().getImage("images/uno.png");
        this.setIconImage(icone);


        //Ajouter un background Image

        accueil = Toolkit.getDefaultToolkit().getImage("images/accueil.png");
        try{
            MediaTracker mt = new MediaTracker(this);
            mt.addImage(accueil,0);
            mt.waitForAll();
        }catch(Exception e){e.printStackTrace();}
        setContentPane(new ContentPane(accueil));


        //Initialisation du bouton Commencer la partie
        JButton boutonCommencerPartie = new JButton("Commencer la partie");
        boutonCommencerPartie.addMouseListener( this);

        //Définition du Layout
        this.setLayout(new GridBagLayout());



        //Zone de texte pour entrer le pseudo
        JLabel nom = new JLabel("Renseignez votre pseudo : \n");
        pseudo.setColumns(10); // On lui donne un nombre de colonnes à afficher

        this.add(nom);
        this.add(pseudo);

        //Ajouter le bouton à la JFrame
        this.getContentPane().add(boutonCommencerPartie);

        //Rendre visible notre fenêtre
        this.setVisible(true);

    }

    public class ContentPane extends JPanel{
        private Image image;
        public ContentPane(Image leFond){super();image=leFond;}
        /* Surcharge de la fonction paintComponent() pour afficher notre image */
        public void paintComponent(Graphics g){
            g.drawImage(image,0,0,largeur, hauteur,null);
        }
    }

    public static JTextField getPseudo() {
        return pseudo;
    }


    public String recupererNom(){
        String name = Accueil.getPseudo().getText();
        return name;
    }


    public static void main(String args[]){
        new Accueil();
    }


    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        String name = recupererNom();
        if(!name.equals("")) { //Si le champ pseudo n'est pas vide
            System.out.println("HELLO mon nom est " + Accueil.getPseudo().getText());
            dispose();
            new PartieEnCours().setVisible(true);
        }
        else{ //Sinon afficher un message d'erreur

        //Boîte du message d'erreur
        JOptionPane jop3 = new JOptionPane();
        jop3.showMessageDialog(null, "Merci de renseigner votre pseudo avant de continuer!", "Erreur", JOptionPane.ERROR_MESSAGE);
        }

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }


}
