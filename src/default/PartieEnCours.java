package Default;

/**
 * *
 * @author sokhnafatoudieng
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.*;
import java.util.ArrayList;


public class PartieEnCours extends JFrame implements MouseListener {

    //Attributs de la classe
    private ArrayList<Joueur> joueurs = new ArrayList <>();

    private JButton pioche; // Initialisation  du JButton pour la pioche
    private JButton aide; // Initialisation du bouton Aide
    private Pioche piocher = new Pioche(); // Création d'une instance de la classe Pioche
    private JPanel joueurReel;
    private JButton carte;
    private ImageIcon carteTalon = new ImageIcon("images/Cartes/" +piocher.initialiserTalon().getNomImage());


    //Constructeur de la classe
    PartieEnCours() {
        this.joueurs.add(new Joueur(Accueil.getPseudo().getText()));
        this.joueurs.add(new Joueur("Joueur 2"));
        this.joueurs.add(new Joueur("Joueur 3"));
        this.joueurs.add(new Joueur("Joueur 4"));

        initialisationJeu(); // Appel de la fonction initialiserJeu

        //Définition de la vue

        // ***** Définition de la fenêtre de jeu ***** //
        this.setTitle("Uno Lion"); /*Définition du titre de la fenêtre de jeu*/
        this.setSize(1100, 800);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        setResizable(false);
        this.setLayout(new BorderLayout());

        // ***** Définition de la barre des menus - Bouton Aide ***** //
        JPanel menu = new JPanel();
        menu.setLayout(new BorderLayout()); // Affectation d'un setLayout à notre menu
        menu.setBackground(Color.WHITE);
        aide = new JButton("Aide"); // Création du bouton Aide qui va ouvrir une popup où sera affichée les règles du jeu
        menu.add(aide, BorderLayout.EAST); // Ajouter le bouton aide à la barre des menus
        aide.addMouseListener(this);
        this.add(menu, BorderLayout.NORTH); // ajouter la barre des menus à la fenêtre de jeu


        //Création d'un JPanel qui englobera tout le jeu
        JPanel jeu = new JPanel();
        jeu.setLayout(new BorderLayout());

        // ***** Définition de l'espace du plateau de jeu contenant : Pioche - Talon - couleur en cours ***** //
        JPanel plateau = new JPanel();
        plateau.setBackground(Color.WHITE);
        plateau.setLayout(new BorderLayout());
        pioche = new JButton(new ImageIcon("images/pioche.png"));
        pioche.setSize(50, 50);
        pioche.addMouseListener(this);
        repaint();


        JLabel talon = new JLabel(carteTalon);
        talon.setBackground(Color.GRAY);
        JPanel couleurCourante = new JPanel();
        couleurCourante.setBackground(Color.yellow);
        plateau.add(pioche, BorderLayout.WEST);
        plateau.add(talon, BorderLayout.EAST);
        plateau.add(couleurCourante, BorderLayout.CENTER);
        jeu.add(plateau, BorderLayout.CENTER);


        this.add(jeu, BorderLayout.CENTER);


        // ***** Définition de l'espace du joueur 0 ***** //
        JPanel joueur0 = new JPanel();
        joueur0.setLayout(new FlowLayout(FlowLayout.CENTER));
        JLabel nomJoueur0 = new JLabel(joueurs.get(1).nomJoueur);
        nomJoueur0.setHorizontalAlignment(SwingConstants.CENTER);
        joueur0.add(nomJoueur0, BorderLayout.NORTH);

        for (int i = 0; i < joueurs.get(1).getCartesJoueur().size(); i++) {
            JLabel carte = new JLabel(new ImageIcon("images/interface/cartesNord.png"));
            carte.setMaximumSize(new Dimension(65, 100));
            carte.setMinimumSize(new Dimension(65, 100));
            joueur0.add(carte);
        }

        jeu.add(joueur0, BorderLayout.NORTH);


        // ***** Définition de l'espace du joueur réel ***** //
        joueurReel = new JPanel();
        joueurReel.setLayout(new FlowLayout(FlowLayout.CENTER));


        joueurReel.removeAll();
        afficherCartesJoueurCourant();



        jeu.add(joueurReel, BorderLayout.SOUTH);


        // ***** Définition de l'espace du joueur 2 ***** //
        JPanel joueur2 = new JPanel();
        joueur2.setLayout(new BorderLayout());

        JPanel casenom2 = new JPanel();
        casenom2.setLayout(new BorderLayout());
        JLabel nomjoueur2 = new JLabel(joueurs.get(2).nomJoueur);
        nomjoueur2.setHorizontalAlignment(SwingConstants.CENTER);
        casenom2.add(nomjoueur2);
        joueur2.add(casenom2, BorderLayout.WEST);


        JPanel casecartes2 = new JPanel();
        casecartes2.setLayout(new BoxLayout(casecartes2, BoxLayout.Y_AXIS));
        for (int i = 0; i < joueurs.get(2).getCartesJoueur().size(); i++) {
            JLabel carte = new JLabel(new ImageIcon("images/interface/cartesOuest.png"));
            carte.setMaximumSize(new Dimension(100, 65));
            carte.setMinimumSize(new Dimension(100, 65));
            casecartes2.add(carte);
        }
        casecartes2.setAlignmentY(CENTER_ALIGNMENT);
        joueur2.add(casecartes2, BorderLayout.CENTER);
        jeu.add(joueur2, BorderLayout.EAST);


        // ***** Définition de l'espace du joueur 3 ***** //

        JPanel joueur3 = new JPanel();
        joueur3.setLayout(new BorderLayout());

        JPanel casenom3 = new JPanel();
        casenom3.setLayout(new BorderLayout());
        JLabel nomjoueur3 = new JLabel(joueurs.get(3).nomJoueur);
        nomjoueur3.setHorizontalAlignment(SwingConstants.CENTER);
        casenom3.add(nomjoueur3);
        joueur3.add(casenom3, BorderLayout.EAST);


        JPanel casecartes3 = new JPanel();
        casecartes3.setLayout(new BoxLayout(casecartes3, BoxLayout.Y_AXIS));
        for (int i = 0; i < joueurs.get(3).getCartesJoueur().size(); i++) {
            JLabel carte = new JLabel(new ImageIcon("images/interface/cartesEst.png"));
            carte.setMaximumSize(new Dimension(100, 65));
            carte.setMinimumSize(new Dimension(100, 65));
            casecartes3.add(carte);
        }


        casecartes3.setAlignmentY(CENTER_ALIGNMENT);
        joueur3.add(casecartes3, BorderLayout.CENTER);
        jeu.add(joueur3, BorderLayout.WEST);


        //Changer l'image de l'icone
        Image icone = Toolkit.getDefaultToolkit().getImage("images/uno.png");
        this.setIconImage(icone);


    }

    private void initialisationJeu() {


        for (Joueur joueur : joueurs) {
            piocher.distribuerNCartes(7, joueur);
        }



    }


    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {


        //Boîte du message d'information
        if (e.getSource() == aide) {
            JOptionPane jop1 = new JOptionPane();
            String regles = "";

            try {
                regles = readFile("texte/aide.txt");
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            jop1.showMessageDialog(null, regles, "Information", JOptionPane.INFORMATION_MESSAGE);
        }

        if (e.getSource() == pioche) {
            piocher.distribuerNCartes(1, joueurs.get(0));
            joueurReel.removeAll();
            afficherCartesJoueurCourant();
        }


        if (e.getSource() == carte) {
            System.out.println(((JButton) e.getSource()).getIcon().toString());
            carteTalon = new ImageIcon(((JButton) e.getSource()).getIcon().toString());
            //Supprimer la carte de la liste de cartes du joueur

        }


    }

    private String readFile(String file) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String line = null;
        StringBuilder stringBuilder = new StringBuilder();
        String ls = System.getProperty("line.separator");

        try {
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(ls);
            }

            return stringBuilder.toString();
        } finally {
            reader.close();
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }


    public void afficherCartesJoueurCourant(){
        JLabel nomjoueurReel = new JLabel(joueurs.get(0).nomJoueur);
            nomjoueurReel.setHorizontalAlignment(SwingConstants.CENTER);
            joueurReel.add(nomjoueurReel,BorderLayout.NORTH);

            for(int i = 0; i<joueurs.get(0).getCartesJoueur().size();i++) {
            carte = new JButton(new ImageIcon("images/Cartes/" + joueurs.get(0).getCartesJoueur().get(i).getNomImage()));
            carte.setMaximumSize(new Dimension(65, 100));
            carte.setMinimumSize(new Dimension(65, 100));
            joueurReel.add(carte);
            carte.addMouseListener(this);
        }
    }
}
